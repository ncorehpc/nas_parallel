NAS Parallel Benchmarks 3.3
===========================

https://www.nas.nasa.gov/software/npb.html

Overview
--------

This is a C and OpenMP version designed for small processors and SoCs.

Descriptions of the benchmark problem sizes are here:

https://www.nas.nasa.gov/software/npb_problem_sizes.html

Problem sizes range from A-S

Building the Suite
------------------

make ARCH=<arch> suite

where `arch` is one of:

riscv64 (X280)
x86_x64 (the native machine type)
aarch64 (zcu102 A-53)

Problem Sizes
-------------
Problem sizes are determined at compile time.  The 'suite' compile target problem sizes can be set in:

config/suite.def

for each benchmark.

Running Benchmarks
------------------

The benchmarks are in ./bin. Run the desired benchmark from the command line.

Test Scripts
------------

To build and run the benchmarks with with type 'A' sizes for x86_64:

./test_scripts/run_test

Note:

./test_scripts/comp and ./test/scripts/runit can be modified to buld cross compiled versions also

Comments/Questions to: thea@ncorehpc.com
